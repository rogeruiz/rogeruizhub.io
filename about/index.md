---
layout: page
title: About
---

I am a self-taught software engineer that found my place in web development by
coming in through the window, specifically the browser window. Coming from a
traditional graphic design background, CSS and systems design were two things
that felt as natural as color theory and typography. As I learned more about the
history of and fundamentals of programming, I made my transition to full-stack
web development with a strong focus on front-end, back-end, infrastructure,
and automation.

I view the projects I work on as umbrellas that cover or overlap many aspects of
how people interact with the government. Whether it's interagency interactions
like the [Open Opportunities platform] [openopps] or the [Federal Front Door]
[ffd] welcoming civilians into necessary interactions with the government, I see
the projects I am working on as a web that helps bring down walls around
information or shed light into perceived black box processes. My main focus is
on getting government agencies into the latest technologies and best practices
with [cloud.gov][cg-home] and [Draft US Web Design Standards][uswds].

[openopps]: https://github.com/18F/openopps-platform "Open Opportunities Github Repo"
[ffd]: https://labs.usa.gov/ "Federal Front Door at USA.gov"
[uswds]: https://standards.usa.gov/ "Draft US Web Design Standards"
[cg-home]: https://cloud.gov/ "A Platform as a Service for government teams"

After years of working in the advertising industry, I wanted a change that would
make a real impact in the world with software and services. Instead of helping people
click-through to more content or change a ad-specific communication preference,
I want to change the way people get to interact with government services whether
it's tools used by federal agencies or by the American people.

### Experience
<ul class="exp-list">
  {% for exp in site.data.experiences %}
  <li>
    <span class="exp-name">{{ exp.name }}</span>
    <span class="exp-dates" title="{{ exp.length }}">{{ exp.dates }}</span>
    <p>{{ exp.title }}</p>
  </li>
  {% endfor %}
</ul>

<a class="no-highlight" href="http://rogerisworking.tumblr.com">![Roger animated]({{ site.baseurl }}/img/about-self.gif "Typos, fam...")</a>
